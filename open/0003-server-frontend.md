- Feature Name: server-frontend  (**Overseer**)
- Start Date: 2018-07-15
- Tracking Issue: 0003

# 1. Summary
[summary]: #summary

The server frontend feature is a graphical user interface using web technologies to provide monitoring for the game server jobs.

# 2. Motivation
[motivation]: #motivation

The goal of of this feature is to provide a graphical user interface tool for server-side jobs monitoring in order to watch the server tasks and get statistics about them for visualization and/or optimization purpose.

# 3. Guide-level explanation
[guide-level-explanation]: #guide-level-explanation

The server frontend, named **Overseer**, has for purpose to introduce a visualization tool of the differents tasks running on the Veloren game server. This feature is an interface that displays the jobs currently running on the game server. It is possible to access the job details in order to get some specific statistics (e.g. uptime, resources used, ...). There also is a configuration page to edit server parameters (e.g. network port, maximum active players, ...).

This feature is for the game server management only and should not have any direct relation to the game. The interaction between the game server and the server frontend should be through an interface that will be details further in this document. There still is some works on the game server since it is needed for the game server jobs to interact with the server frontent (see 4.3).

# 4. Reference-level explanation
[reference-level-explanation]: #reference-level-explanation

The server frontend feature should not directly interact with any part of the main Veloren game. Any server jobs should register at start to a message broker service which is part of the server frontend. In case the server frontend is not running, the jobs should not try to register to the service, but if the server frontend is started while the jobs are already running, the jobs should then register to the service when the server frontend is fully up and running.

The game server jobs register to a message broker service which is part of the server frontend feature. This message broker feeds differents channels for each type of informations to be provided to the interface and each messages going through a channel has a payload that indicates which game server job provided it. Each channel publishes messages as soon as the message broker feeds one to the channel. For each channel, a client is acting as a subscriber and receive the channel messages. Each clients is represented as an asynchronous actor that feeds the interface of pre-processed messages about the job. Each job in the interface also is an an asynchronous actor that requires frequent updates to be considered still alive. A job actor stores all informations provided by client actors to process them as statistics. A job actor should not be erased when a client actor signal the end of the game server job's linked to it or if the job actor timeout to still get the informations linked to it.

[Diagram required for above paragraph]

The server frontend take place as a web server using the [Rocket](https://github.com/SergioBenitez/Rocket) crate that display a web interface. The actors are managed using the [Actix](https://github.com/actix/actix) crate. All other part of this feature are developed from scratch.

## 4.1 Actors

There is two types of actors in the server frontend:

- Clients 
- Jobs

### 4.1.1 Clients

- A client actor is a subscriber to the message broker channels.
- A client actor upon receiving a message process it and feed it to the identified job actor.

### 4.1.2 Jobs

- A job actor is a server side job overseer. 
- A job actor represents one server-side tasks, be it running or dead. 
- A job actor regroups all informations about the server-side job it oversee. 
- A job actor computes statistics with those informations.
- A job actor timeouts if there was no updates from a client actor over a predetermined time.

The informations a job actor stores and the statistics a job actor computes are in 4.2.2.

## 4.2 Interface

The interface is split in three views:

- Job list
- Job details
- Server configuration

### 4.2.1 Job list

The job list view displays all jobs that are currently running or that are dead. Filters are available to be able to select what needs to be displayed and lessen the visual workload of the interface. Filters are:

- Lifecycle (Running/Dead)
- Uptime (Ascending/Descending)
- Type (To be defined)
- (More are to be defined)

The default filter is running jobs, descending uptime and any type.

### 4.2.2 Job details

The job details view displays all informations and statistics about a specific selected job. The avalaible informations are:

- Uptime
- Resources (RAM/CPU usage/Thread #)
- Lifecycle (Running/Dead)
- (More are to be defined)

The statistics are:

- (More are to be defined)

### 4.2.3 Server configuration

The server configuration view displays all server parameters that are alterable in order to provide a graphical user interface for server configuration. Those parameters are:

- Server port
- Maximum active players
- (More are to be defined)

## 4.3 Message broker service

To be discussed because it involves a lot of technical issues.

# 5. Drawbacks
[drawbacks]: #drawbacks

Monitoring jobs may not be useful in the case of the Veloren project since it depends on the architecture of the game server and its scale.

# 6. Rationale and alternatives
[alternatives]: #alternatives

To be discussed because I have no idea right now.

# 7. Prior art
[prior-art]: #prior-art

To be discussed because I have no knowledge about this kind of state of the art.

# 8. Unresolved questions
[unresolved]: #unresolved-questions

More details about channels, informations and statistics should be discussed and approved by the contributors.

The message broker service is not decided yet and need further discussion with the contributors.

The design may not be the best one and should be improved related to the goal of this feature.